﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace MinecraftManager
{
    [Serializable]
    public class World
    {
        //general info
        private System.Drawing.Bitmap _Icon;
        private string _Name;
        private string _Description;
        private string _Seed;

        //getters and setters
        public System.Drawing.Bitmap Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string Seed
        {
            get { return _Seed; }
            set { _Seed = value; }
        }
        
        //methods
        public void Save()
        {
            string filename = _Name + ".mmw";
            string path= Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MinecraftManager/"+filename);

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream writerFileStream = new FileStream(path, FileMode.Create, FileAccess.Write);
            
            formatter.Serialize(writerFileStream, this);
            
            writerFileStream.Close();
        }

        public World Load(string filename)
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MinecraftManager/" + filename);

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream readerFileStream = new FileStream(path,FileMode.Open, FileAccess.Read);

            World loaded = (World)formatter.Deserialize(readerFileStream);

            readerFileStream.Close();

            return loaded;
        }
    }
}

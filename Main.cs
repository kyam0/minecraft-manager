﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace MinecraftManager
{
    public partial class Main : Form
    {
        World loaded;

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Show(object sender, EventArgs e)
        {
            /// create MinecraftManager folder in appdata IF it doesnt already exist
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string ADfolder = Path.Combine(path, "MinecraftManager");
            Directory.CreateDirectory(ADfolder); //this only runs if the folder doesnt exist

            CheckWorlds();
            CheckLoaded();
        }

        private void CheckWorlds()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string ADfolder = Path.Combine(path, "MinecraftManager");

            // check if any worlds exist (in appdata folder)
            string[] files = System.IO.Directory.GetFiles(ADfolder);
            if (files.Length == 0)
            {
                NewWorld();
            }
            else
            {
                OpenWorld();
            }
        }

        private void CheckLoaded()
        {
            // check if a world has been loaded (enable tabs if yes)
            if (loaded == null)
                tabControl1.Enabled = false;
            else
            {
                tabControl1.Enabled = true;
                UpdateDisplay();
            }
        }

        private void OpenWorld()
        {
            using (Open_World ow = new Open_World())
            {
                if (ow.ShowDialog() == DialogResult.OK)
                {
                    loaded = ow.passback;
                }
                else
                {
                    NewWorld();
                }
            }
        }

        private void NewWorld()
        {
            using (New_World nw = new New_World())
            {
                if (nw.ShowDialog() == DialogResult.OK)
                {
                    OpenWorld();
                }
            }
        }

        private void UpdateDisplay()
        {
            this.Text = "Minecraft Manager - " + loaded.Name;
            pictureBox2.Image = loaded.Icon;
            label1.Text = loaded.Name;
            textBox4.Text = loaded.Seed;
            richTextBox1.Text = loaded.Description;
        }

        private void button3_Click(object sender, EventArgs e) // save updated description
        {
            loaded.Description = richTextBox1.Text;
            loaded.Save();
        }

        private void newWorldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewWorld();
        }

        private void openWorldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenWorld();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MinecraftManager
{
    public partial class Open_World : Form
    {
        World world;
        public World passback
        {
            set { world = value; }
            get { return world; }
        }

        public Open_World()
        {
            InitializeComponent();

            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "MinecraftManager");
            DirectoryInfo d = new DirectoryInfo(path);
            FileInfo[] Files = d.GetFiles("*.mmw");
            foreach (FileInfo file in Files)
            {
                listBox1.Items.Add(file.Name);
            }

            listBox1.SelectedIndex = 0;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            World selected = new World();
            selected = selected.Load((string)listBox1.SelectedItem);

            pictureBox1.Image = selected.Icon;
            label3.Text = selected.Name;
            richTextBox1.Text = selected.Description;
        }

        private void button1_Click(object sender, EventArgs e) // load selected world
        {
            world = new World();
            world = world.Load((string)listBox1.SelectedItem);
            this.DialogResult = DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e) // create new world
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}

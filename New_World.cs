﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

namespace MinecraftManager
{
    public partial class New_World : Form
    {
        public New_World()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog();
                Bitmap bmp = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = bmp;
            }
            catch
            {

            }
            
        }

        private void button3_Click(object sender, EventArgs e) // cancel button
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) // submit button
        {
            Bitmap icon = (Bitmap)pictureBox1.Image;
            String name = textBox1.Text;
            String desc = richTextBox1.Text;
            String seed = textBox2.Text;

            World world = new World
            {
                Icon = icon,
                Name = name,
                Description = desc,
                Seed = seed
            };

            world.Save();
            this.DialogResult = DialogResult.OK;
        }
    }
}
